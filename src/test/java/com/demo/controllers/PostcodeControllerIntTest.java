package com.demo.controllers;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

/**
 * Integration test for {@link PostcodeController} test to ensure correctness of routing and JSON serialisation
 */
@SpringBootTest
@AutoConfigureMockMvc
public class PostcodeControllerIntTest {

	@Autowired
    private MockMvc mvc;
	
	@Test
	public void testSaveAndQuery() throws Exception {
		var postContent = "6000,Perth\n6012,Mosman Park\n6010,Swanbourne";
		
		mvc.perform(post("/save").content(postContent).contentType(MediaType.APPLICATION_JSON));
		
		mvc.perform(get("/names").queryParam("from", "1").queryParam("to", "6011")
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(status().isOk())
			      .andExpect(jsonPath("$.charCount", is("PerthSwanbourne".length())))
			      .andExpect(jsonPath("$.names", hasSize(2)))
			      .andExpect(jsonPath("$.names[0]", is("Perth")))
			      .andExpect(jsonPath("$.names[1]", is("Swanbourne")));
	}
}
