package com.demo.controllers;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.jupiter.api.Test;

import com.demo.model.Postcode;
import com.demo.repositories.PostcodeInMemoryRepository;
import com.demo.repositories.PostcodeRepository;

/**
 * Unit tests for {@link PostcodeController}
 */
public class PostcodeControllerTest {

	private PostcodeRepository repo = new PostcodeInMemoryRepository();
	private PostcodeController controller = new PostcodeController(repo);
	
	/**
	 * Initialises the state of the test fixture.
	 * @param postcodes The postcodes to store in the repository.
	 */
	private void init(Postcode ...postcodes) {
		repo.saveAll(asList(postcodes));
	}

	@Test
	public void testSavePostcode() {
		controller.savePostcodes("6000,Perth");

		assertThat(repo.findBetween(6000, 6000), contains(new Postcode(6000, "Perth")));
	}

	@Test
	public void testQueryPostcodesFiltersByFromTo() {
		init(new Postcode(6000, "Perth"), new Postcode(6012, "Mosman Park"), new Postcode(6010, "Swanbourne"));
		
		var result = controller.findNames(6001, 6011);
		assertThat(result.names, contains("Swanbourne"));
	}
	
	@Test
	public void testQueryPostcodesSortsByName() {
		init(new Postcode(6000, "Perth"), new Postcode(6012, "Mosman Park"), new Postcode(6010, "Swanbourne"));
		
		var result = controller.findNames(6000, 6012);
		assertThat(result.names, contains("Mosman Park", "Perth", "Swanbourne"));
	}
	
	@Test
	public void testQueryPostcodesCharacterCount() {
		init(new Postcode(6000, "Perth"), new Postcode(6010, "Swanbourne"));
		
		var result = controller.findNames(6000, 6010);
		assertThat(result.charCount, is("PerthSwanbourne".length()));
	}

}
