package com.demo.repository;

import org.springframework.beans.factory.annotation.Autowired;

import com.demo.repositories.PostcodeDatastoreRepository;
import com.demo.repositories.PostcodeRepository;

/**
 * Unit tests for {@link PostcodeDatastoreRepository}
 */
// Not implemented in this demo.
// @SpringBootTest
public class PostcodeDatastoreRepositoryIntTest /* extends BasePostcodeRepositoryTest */ {

	@Autowired
    private PostcodeDatastoreRepository repository;
	
	// @Override
	protected PostcodeRepository getPostcodeRepository() {
		return repository;
	}

}
