package com.demo.repository;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;

import com.demo.model.Postcode;
import com.demo.repositories.PostcodeRepository;

/**
 * Base test class for the PostcodeRepository so the tests can be run on different implementations of the repository.
 * 
 * This ensures the in-memory version is a high fidelity implementation of the real datastore repository.
 * So unit tests using the in-memory implementation behave as close as possible to production database.
 */
public abstract class BasePostcodeRepositoryTest {

	protected abstract PostcodeRepository getPostcodeRepository();
	
	
	@Test
	public void testSavingDuplicatesShouldReturnDistinct() {
		List<Postcode> postcodes = asList(new Postcode(6153, "Applecross"));
		getPostcodeRepository().saveAll(postcodes);
		getPostcodeRepository().saveAll(postcodes);
		
		var findResults = getPostcodeRepository().findBetween(6153, 6153);

		assertThat(findResults, equalTo(postcodes));
	}
	
	
	@Test
	public void testAllowMultipleNamesPerPostcode() {
		var applecross = new Postcode(6153, "Applecross");
		var ardross = new Postcode(6153, "Ardross");
		getPostcodeRepository().saveAll(asList(applecross, ardross));

		var findResults = getPostcodeRepository().findBetween(6153, 6153);

		assertThat(findResults, containsInAnyOrder(applecross, ardross));
	}

	/**
	 * Test the filtering of the results by the from/to range
	 */
	@Test
	public void testFindBetween_FromTo() {
		var repository = getPostcodeRepository();
		
		var perth = new Postcode(6000, "Perth");
		var mosmanPark = new Postcode(6012, "Mosman Park");
		var applecross = new Postcode(6153, "Applecross");
		repository.saveAll(asList(perth, applecross, mosmanPark));

		var findOutsideRange = repository.findBetween(5000, 5999);
		assertThat(findOutsideRange, empty());

		var findTo = repository.findBetween(5000, 6011);
		assertThat(findTo, contains(perth));

		var findToInclusive = repository.findBetween(5000, 6153);
		assertThat(findToInclusive, containsInAnyOrder(perth, mosmanPark, applecross));

		var findFromInclusive = repository.findBetween(6153, 6999);
		assertThat(findFromInclusive, containsInAnyOrder(applecross));
	}
}
