package com.demo.repository;

import com.demo.repositories.PostcodeInMemoryRepository;
import com.demo.repositories.PostcodeRepository;

/**
 * Unit tests for {@link PostcodeInMemoryRepository}
 */
public class PostcodeInMemoryRepositoryTest extends BasePostcodeRepositoryTest {

	private PostcodeRepository postcodeRepository = new PostcodeInMemoryRepository();
	
	@Override
	protected PostcodeRepository getPostcodeRepository() {
		return postcodeRepository;
	}
}
