package com.demo.repositories;

import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.stereotype.Repository;

import com.demo.model.Postcode;

@Repository
public class PostcodeInMemoryRepository implements PostcodeRepository {

	private Set<Postcode> postcodes = ConcurrentHashMap.newKeySet();

	@Override
	public void saveAll(List<Postcode> postcodes) {
		this.postcodes.addAll(postcodes);
	}

	@Override
	public List<Postcode> findBetween(int postcodeFrom, int postcodeTo) {
		return postcodes.stream()
				.filter(postcode -> postcode.postcode() >= postcodeFrom && postcode.postcode() <= postcodeTo)
				.toList();
	}
}
