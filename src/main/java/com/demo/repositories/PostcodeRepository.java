package com.demo.repositories;

import java.util.List;

import com.demo.model.Postcode;

public interface PostcodeRepository  {

	/**
	 * Saves the postcodes.
	 * @param postcodes
	 */
	void saveAll(List<Postcode> postcodes);
	
	/**
	 * Returns the postcodes in the range provided
	 * @param postcodeFrom
	 * @param postcodeTo
	 * @return the postcodes in the range
	 */
	List<Postcode> findBetween(int postcodeFrom, int postcodeTo);
}
