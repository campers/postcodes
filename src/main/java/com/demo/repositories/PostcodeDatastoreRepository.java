package com.demo.repositories;

import java.util.List;

import org.springframework.cloud.gcp.data.datastore.repository.DatastoreRepository;
import org.springframework.cloud.gcp.data.datastore.repository.query.Query;
import org.springframework.context.annotation.Profile;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.demo.model.Postcode;

/**
 * Untested sample implementation of the PostcodeRepository with Datastore.
 * To implement properly would require changing the Postcode record to a normal class, adding a primary key, configuration for datastore (index on postcode) etc
 */
@Repository
@Profile({ "GCP" })
public interface PostcodeDatastoreRepository extends DatastoreRepository<Postcode, Postcode>, PostcodeRepository {

	@Query("SELECT * FROM postcodes WHERE postcode >= @from AND postcode <= @to")
	@Override
	List<Postcode> findBetween(@Param("from") int postcodeFrom, @Param("to") int postcodeTo);
}
