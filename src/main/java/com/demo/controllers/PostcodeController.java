package com.demo.controllers;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toList;

import java.util.List;
import java.util.stream.Stream;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.demo.model.Postcode;
import com.demo.repositories.PostcodeRepository;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * Routes for saving and querying postcodes
 */
@RestController()
@RequestMapping("/")
public class PostcodeController {

	private PostcodeRepository postcodeRepository;

	public PostcodeController(PostcodeRepository postcodeRepository) {
		this.postcodeRepository = postcodeRepository;
	}

	/**
	 * Saves the list of postcodes. The post body must be a list of postcodes in the format POSTCODE,NAME
	 * @param body
	 * @return
	 */
	@PostMapping("/save")
	public void savePostcodes(@RequestBody String body) {
		List<Postcode> postcodes = Stream.of(body.split("\n")).map(Postcode::parseCsv).toList();
		postcodeRepository.saveAll(postcodes);
	}

	/**
	 * @param from the postcode to search from 
	 * @param to the postcode to search to
	 * @return the sorted names of the postcodes in the range, and the total character count of the names.
	 */
	@GetMapping(value="/names")
	public PostcodeQueryResponse findNames(@RequestParam(value = "from") int from, @RequestParam(value = "to") int to) {
		return postcodeRepository.findBetween(from, to).stream()
		.map(Postcode::name)
		.sorted((name1, name2) -> name1.compareTo(name2))
		.collect(collectingAndThen(toList(), PostcodeQueryResponse::new));
	}

	
	@JsonSerialize()
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class PostcodeQueryResponse {
		
		public int charCount = 0;
		public List<String> names;
		
		public PostcodeQueryResponse (List<String> names) {
			this.names = names;
			for (String name : names) {
				this.charCount += name.length();
			}
		}
	}
	
}
