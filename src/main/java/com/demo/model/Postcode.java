package com.demo.model;

import static java.lang.Integer.parseInt;

/**
 * @param postcode the numeric postcode
 * @param name the name/locality of the suburb/town/city
 */
public record Postcode(int postcode, String name) {
	
	/**
	 * Parses a CSV representation
	 * @param csv string which must be in the format POSTCODE,NAME
	 * @return
	 */
	public static Postcode parseCsv(String csv) {
		int commaIndex = csv.indexOf(',');
		if (commaIndex == -1)
			throw new IllegalArgumentException("Must provide a comma seperated value. Was " + csv);
		
		return new Postcode(parseInt(csv.substring(0, commaIndex)), csv.substring(commaIndex + 1, csv.length()));
	}
}