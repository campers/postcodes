APP_NAME=postcodes
REGION=australia-southeast1
GCP_PROJECT_ID=your-project-id
TAG=latest

./mvnw com.google.cloud.tools:jib-maven-plugin:1.8.0:build  -Dimage=gcr.io/$GCP_PROJECT_ID/$APP_NAME

gcloud run deploy $APP_NAME --project=$GCP_PROJECT_ID --image gcr.io/$GCP_PROJECT_ID/$APP_NAME:$TAG --platform managed --allow-unauthenticated --region=$REGION
